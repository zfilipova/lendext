﻿using LendExt.Data.Configurations;
using LendExt.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;

namespace LendExt.Data
{
    class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Application> Applications { get; set; }
        public DbSet<ApplicationStatus> ApplicationStatus { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
        public DbSet<ClosedApplication> ClosedApplications { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<EmailAttachment> EmailAttachments { get; set; }
        public DbSet<EmailBody> EmailBodies { get; set; }
        public DbSet<EmailStatus> EmailStatus { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Right> Rights { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ApplicationConfigurations());
            modelBuilder.ApplyConfiguration(new ApplicationStatusConfigurations());
            modelBuilder.ApplyConfiguration(new UserConfigurations());
            modelBuilder.ApplyConfiguration(new AuditLogConfigurations());
            modelBuilder.ApplyConfiguration(new ClosedApplicationConfigurations());
            modelBuilder.ApplyConfiguration(new CustomerConfigurations());
            modelBuilder.ApplyConfiguration(new EmailConfigurations());
            modelBuilder.ApplyConfiguration(new EmailAttachmentConfigurations());
            modelBuilder.ApplyConfiguration(new EmailBodyConfigurations());
            modelBuilder.ApplyConfiguration(new EmailStatusConfigurations());
            modelBuilder.ApplyConfiguration(new PartnerConfigurations());
            modelBuilder.ApplyConfiguration(new RightConfigurations());           
        }
        // to be removed later
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=127.0.0.1;Database=test2DB;Username=postgres;Password=admin");

        public override int SaveChanges()
        {
            var forDeletion = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted);

            foreach (EntityEntry item in forDeletion)
            {
                item.State = EntityState.Modified;
                var baseItem = (BaseEntity)item.Entity;
                baseItem.IsDeleted = true;
                baseItem.DeletedOn = DateTime.Now;
            }
            return base.SaveChanges();
        }
    }
}
