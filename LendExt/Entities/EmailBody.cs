﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LendExt.Data.Entities
{
    public class EmailBody : BaseEntity
    {
        public string Text { get; set; }
    }
}
