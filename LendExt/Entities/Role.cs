﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LendExt.Data.Entities
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }
    }
}
