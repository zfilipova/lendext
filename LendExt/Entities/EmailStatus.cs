﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LendExt.Data.Entities
{
    public class EmailStatus : BaseEntity
    {
        public string Name { get; set; }
    }
}
