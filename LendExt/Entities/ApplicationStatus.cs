﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LendExt.Data.Entities
{
    public class ApplicationStatus : BaseEntity
    {
        public string Name { get; set; }
    }
}
