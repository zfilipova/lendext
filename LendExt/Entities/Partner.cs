﻿using System.Collections.Generic;

namespace LendExt.Data.Entities
{
    public class Partner : BaseEntity
    {
        public ICollection<string> Phones { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string ContactPersonName { get; set; }
    }
}
