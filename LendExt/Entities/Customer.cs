﻿using System.Collections.Generic;

namespace LendExt.Data.Entities
{
    public class Customer : BaseEntity
    {
        public ICollection<string> Phones { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
    }
}
