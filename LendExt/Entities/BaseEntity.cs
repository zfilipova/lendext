﻿using System;

namespace LendExt.Data.Entities
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            Id = new Guid().ToString();
        }
        public string Id { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
