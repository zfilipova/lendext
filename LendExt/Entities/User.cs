﻿using System.Collections.Generic;

namespace LendExt.Data.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public ICollection<string> Phones { get; set; }
        public string RoleId { get; set; }
        public Role Role { get; set; }
    }
}
