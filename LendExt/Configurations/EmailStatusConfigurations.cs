﻿using LendExt.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LendExt.Data.Configurations
{
    public class EmailStatusConfigurations : IEntityTypeConfiguration<EmailStatus>
    {
        public void Configure(EntityTypeBuilder<EmailStatus> builder)
        {
            builder.Property(b => b.IsDeleted).HasDefaultValue(false).IsRequired();
            builder.Property(b => b.CreatedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAdd().IsRequired();
            builder.Property(b => b.ModifiedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnUpdate().IsRequired();
        }
    }
}
